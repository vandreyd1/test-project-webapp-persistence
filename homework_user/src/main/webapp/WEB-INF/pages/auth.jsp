<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: tasha
  Date: 15.11.17
  Time: 20:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Auth</title>
</head>
<body>
  <spring:form method="post"
               modelAttribute="userFromAuth"
               action="/web-context/users/sayHello">
      <spring:input path="login"/>
      <spring:input path="password"/>
      <%--<spring:input path="email"/>--%>
      <spring:button>CHECK</spring:button>
  </spring:form>
</body>
</html>

package com.controller;


import com.model.User;
import com.repository.UserManagerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller("userController")
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserManagerRepository userManagerRepository;

    public UserManagerRepository getUserManagerRepository() {
        return userManagerRepository;
    }

    public void setUserManagerRepository(UserManagerRepository userManagerRepository) {
        this.userManagerRepository = userManagerRepository;
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public @ResponseBody
    List<User> getAllUsers() {
        return userManagerRepository.getAllUsers();
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView getSayCongratsPage() {
//        User testUser = new User();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("userFromServer", new User());
        modelAndView.setViewName("users");
        return modelAndView;
    }

    @RequestMapping(value = "sayCongrats", method = RequestMethod.POST)
    public @ResponseBody
    String sayCongratsForUser(@ModelAttribute("userFromServer") User user) {
        String login = user.getLogin();
        userManagerRepository.getUserRepository().saveAndFlush(user);
        return "Congrats " + login + ", you're registered!";//String.format("hello %s", name);
    }


    @RequestMapping(value = "/auth", method = RequestMethod.GET)
    public ModelAndView getSayHelloPage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("userFromAuth", new User());
        modelAndView.setViewName("auth");
        return modelAndView;
    }

    @RequestMapping(value = "sayHello", method = RequestMethod.POST)
    public @ResponseBody
    String sayHelloForUser(@ModelAttribute("userFromServer") User user) {

        for (int i = 0; i < (getAllUsers().toArray().length); i++) {
            User userDB = (User) getAllUsers().toArray()[i];
            if (userDB.equals(user)) {
                return "Hello " + userDB.getLogin() + ", you're logged in!\n" +
                        "Your email is " + userDB.getEmail();
            }
        }
        throw new NullPointerException();
    }
}



//        if(getAllUsers().contains(user)){
//            return "Hello " + login + ", you're logged in!\nYour email is "+email;
//        }
//        }else{
//            throw new NullPointerException();
//        }
//    }
/*

        @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody String getAllUsers() {
        return Arrays.toString(userManagerRepository.getAllUsers().toArray());
    }
 */